#!/usr/bin/env sh

www_path=$1

if [ -z $www_path ]; then
  echo "need WWW_PATH!"
  exit 1
fi

html=$www_path/html

ls $html
if [ 0 != $? ]; then
  mkdir -p $html
fi

rm -rf $html/*
mv ./dist/* $html