import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/admin/areas',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/admin/areas' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/admin/areas',
    method: 'put',
    data
  })
}

export function sync() {
  return request({
    url: 'api/admin/areas/sync',
    method: 'get'
  })
}

export function check(id) {
  return edit({ id, checked: true })
}

export function merge(data) {
  return request({
    url: 'api/admin/areas/conflict',
    method: 'put',
    data
  })
}

export function getAllConflicts() {
  return request({
    url: 'api/admin/areas/conflict',
    method: 'get'
  })
}
